Object.defineProperty(exports, "__esModule", {
    value: !0
}), exports.SM4CryptECBWithPKCS7Padding = w, exports.SM4CryptECBWithPKCS7PaddingKeys = A, 
exports.SM4CryptECBWithPKCS7PaddingKey = C, exports.encrypt = S, exports.decrypt = k, 
exports.encrypts = m, exports.decrypts = P, exports.getUUID = U, exports.default = void 0;

var r = [ [ 214, 144, 233, 254, 204, 225, 61, 183, 22, 182, 20, 194, 40, 251, 44, 5 ], [ 43, 103, 154, 118, 42, 190, 4, 195, 170, 68, 19, 38, 73, 134, 6, 153 ], [ 156, 66, 80, 244, 145, 239, 152, 122, 51, 84, 11, 67, 237, 207, 172, 98 ], [ 228, 179, 28, 169, 201, 8, 232, 149, 128, 223, 148, 250, 117, 143, 63, 166 ], [ 71, 7, 167, 252, 243, 115, 23, 186, 131, 89, 60, 25, 230, 133, 79, 168 ], [ 104, 107, 129, 178, 113, 100, 218, 139, 248, 235, 15, 75, 112, 86, 157, 53 ], [ 30, 36, 14, 94, 99, 88, 209, 162, 37, 34, 124, 59, 1, 33, 120, 135 ], [ 212, 0, 70, 87, 159, 211, 39, 82, 76, 54, 2, 231, 160, 196, 200, 158 ], [ 234, 191, 138, 210, 64, 199, 56, 181, 163, 247, 242, 206, 249, 97, 21, 161 ], [ 224, 174, 93, 164, 155, 52, 26, 85, 173, 147, 50, 48, 245, 140, 177, 227 ], [ 29, 246, 226, 46, 130, 102, 202, 96, 192, 41, 35, 171, 13, 83, 78, 111 ], [ 213, 219, 55, 69, 222, 253, 142, 47, 3, 255, 106, 114, 109, 108, 91, 81 ], [ 141, 27, 175, 146, 187, 221, 188, 127, 17, 217, 92, 65, 31, 16, 90, 216 ], [ 10, 193, 49, 136, 165, 205, 123, 189, 45, 116, 208, 18, 184, 229, 180, 176 ], [ 137, 105, 151, 74, 12, 150, 119, 126, 101, 185, 241, 9, 197, 110, 198, 132 ], [ 24, 240, 125, 236, 58, 220, 77, 32, 121, 238, 95, 62, 215, 203, 57, 72 ] ], n = [ 2746333894, 1453994832, 1736282519, 2993693404 ], t = [ 462357, 472066609, 943670861, 1415275113, 1886879365, 2358483617, 2830087869, 3301692121, 3773296373, 4228057617, 404694573, 876298825, 1347903077, 1819507329, 2291111581, 2762715833, 3234320085, 3705924337, 4177462797, 337322537, 808926789, 1280531041, 1752135293, 2223739545, 2695343797, 3166948049, 3638552301, 4110090761, 269950501, 741554753, 1213159005, 1684763257 ];

function e() {
    this.mode = 0, this.sk = [];
}

function o(r, n, t) {
    return n[t] << 24 | n[t + 1] << 16 | n[t + 2] << 8 | n[t + 3];
}

function u(r, n, t) {
    n[t] = r >>> 24, n[t + 1] = r >>> 16, n[t + 2] = r >>> 8, n[t + 3] = r;
}

function a(r, n) {
    return (4294967295 & r) << n | r >>> 32 - n;
}

function i(n) {
    return r[n >>> 4][n % 16];
}

function f(r, n, t, e, f) {
    return r ^ (s = n ^ t ^ e ^ f, p = 0, c = new Uint8Array(4), l = new Array(4), u(s, c, 0), 
    l[0] = i(c[0]), l[1] = i(c[1]), l[2] = i(c[2]), l[3] = i(c[3]), (p = o(0, l, 0)) ^ a(p, 2) ^ a(p, 10) ^ a(p, 18) ^ a(p, 24));
    var s, p, c, l;
}

function s(r, e) {
    var f, s, p, c, l = new Array(4), h = new Array(36), x = 0;
    for (l[0] = o(l[0], e, 0), l[1] = o(l[1], e, 4), l[2] = o(l[2], e, 8), l[3] = o(l[3], e, 12), 
    h[0] = l[0] ^ n[0], h[1] = l[1] ^ n[1], h[2] = l[2] ^ n[2], h[3] = l[3] ^ n[3]; x < 32; x++) h[x + 4] = h[x] ^ (f = h[x + 1] ^ h[x + 2] ^ h[x + 3] ^ t[x], 
    s = void 0, p = void 0, c = void 0, s = 0, p = new Uint8Array(4), c = new Array(4), 
    u(f, p, 0), c[0] = i(p[0]), c[1] = i(p[1]), c[2] = i(p[2]), c[3] = i(p[3]), (s = o(0, c, 0)) ^ a(s, 13) ^ a(s, 23)), 
    r[x] = h[x + 4];
}

function p(r, n, t) {
    var e = 0, a = new Array(36);
    for (a[0] = o(a[0], n, 0), a[1] = o(a[1], n, 4), a[2] = o(a[2], n, 8), a[3] = o(a[3], n, 12); e < 32; ) a[e + 4] = f(a[e], a[e + 1], a[e + 2], a[e + 3], r[e]), 
    e++;
    u(a[35], t, 0), u(a[34], t, 4), u(a[33], t, 8), u(a[32], t, 12);
}

function c(r, n) {
    r.mode = 1, s(r.sk, n);
}

function l(r, n) {
    var t, e;
    for (r.mode = 1, s(r.sk, n), t = 0; t < 16; t++) e = r.sk[31 - t], r.sk[31 - t] = r.sk[t], 
    r.sk[t] = e;
}

function h(r, n, t, e, o) {
    for (var u = 0; t > 0; ) {
        var a = e.slice(u, u + 16), i = new Uint8Array(16);
        p(r.sk, a, i);
        for (var f = 0; f < 16; f++) o[u + f] = i[f];
        u += 16, t -= 16;
    }
}

function x(r) {
    for (var n = r.length / 2, t = "", e = new Array(n), o = 0; o < n; o++) t = r.slice(2 * o, 2 * (o + 1)), 
    e[o] = parseInt(t, 16) || 0;
    return e;
}

var v = function(r) {
    var n, t, e = new Array();
    n = r.length;
    for (var o = 0; o < n; o++) (t = r.charCodeAt(o)) >= 65536 && t <= 1114111 ? (e.push(t >> 18 & 7 | 240), 
    e.push(t >> 12 & 63 | 128), e.push(t >> 6 & 63 | 128), e.push(63 & t | 128)) : t >= 2048 && t <= 65535 ? (e.push(t >> 12 & 15 | 224), 
    e.push(t >> 6 & 63 | 128), e.push(63 & t | 128)) : t >= 128 && t <= 2047 ? (e.push(t >> 6 & 31 | 192), 
    e.push(63 & t | 128)) : e.push(255 & t);
    return e;
}, g = function(r) {
    var n = 0, t = r.length;
    if (t % 2 != 0) return r;
    t /= 2;
    for (var e = new Array(), o = 0; o < t; o++) {
        var u = r.substr(n, 2), a = parseInt(u, 16);
        e.push(a), n += 2;
    }
    return e;
}, y = function(r) {
    for (var n = "", t = 0; t < r.length; t++) {
        var e = r[t].toString(16);
        1 == e.length && (e = "0".concat(e)), n += e;
    }
    return n;
}, d = function(r) {
    if ("string" == typeof r) return r;
    for (var n = "", t = r, e = 0; e < t.length; e++) {
        var o = t[e].toString(2), u = o.match(/^1+?(?=0)/);
        if (u && 8 == o.length) {
            for (var a = u[0].length, i = t[e].toString(2).slice(7 - a), f = 1; f < a; f++) i += t[f + e].toString(2).slice(2);
            n += String.fromCharCode(parseInt(i, 2)), e += a - 1;
        } else n += String.fromCharCode(t[e]);
    }
    return n;
};

function w(r, n) {
    if (null == r || null == r || "" == r) return r;
    var t = "dbd313f58f3a11eabcf8e86a642e4b8a";
    if (32 !== t.length) return "";
    var o = null, u = (o = 1 === n ? v(r) : g(r)).length;
    if (1 === n) for (var a = 16 - u % 16, i = 0; i < a; i++) o.push(a);
    var f = new e(), s = x(t);
    1 === n ? c(f, s) : l(f, s);
    var p = new Array(o.length);
    if (h(f, 0, o.length, o, p), 0 === n) for (var w = p[p.length - 1], A = 0; A < w; A++) p.pop();
    return 1 === n ? y(p) : d(p);
}

function A(r, n, t) {
    if (32 !== t.length) return console.log("传入密钥[" + t + "]长度不为32位"), "";
    var o = null, u = (o = 1 === n ? v(r) : g(r)).length;
    if (1 === n) for (var a = 16 - u % 16, i = 0; i < a; i++) o.push(a);
    var f = new e(), s = x(t);
    1 === n ? c(f, s) : l(f, s);
    var p = new Array(o.length);
    if (h(f, 0, o.length, o, p), 0 === n) for (var w = p[p.length - 1], A = 0; A < w; A++) p.pop();
    return 1 === n ? y(p) : d(p);
}

function C(r, n, t) {
    if (32 !== t.length) return console.log("传入密钥[" + t + "]长度不为32位"), "";
    var o = null, u = (o = 1 === n ? v(r) : g(r)).length;
    if (1 === n) for (var a = 16 - u % 16, i = 0; i < a; i++) o.push(a);
    var f = new e(), s = x(t);
    1 === n ? c(f, s) : l(f, s);
    var p = new Array(o.length);
    if (h(f, 0, o.length, o, p), 0 === n) for (var w = p[p.length - 1], A = 0; A < w; A++) p.pop();
    return 1 === n ? y(p) : d(p);
}

function S(r) {
    return w(r, 1);
}

function k(r) {
    return w(r, 0);
}

function m(r, n) {
    return A(r, 1, n);
}

function P(r, n) {
    return C(r, 0, n);
}

function U() {
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(r) {
        return ("x" === r ? 16 * Math.random() | 0 : 8).toString(16);
    }).replace(/[-]/g, "");
}

var b = {
    encrypt: S,
    decrypt: k,
    encrypts: m,
    decrypts: P,
    getUUID: U
};

exports.default = b;