const blueTower = require('./blue-tower');
const { username, password } = require('../config');

const loginModel = {
    username,
    password
}
const code = blueTower.getUUID();
const data = blueTower.encrypts(JSON.stringify(loginModel), code);

module.exports = {
    code,
    data
}
