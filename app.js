const request = require('./request');
const moment = require('moment');
const { data, code } = require('./utils/code');
const { search, group, qq } = require('./config.json');

const admin = {
    data,
    code,
}
const date = moment(Date.now()).format('YYYY-MM-DD');

async function func() {
    const token = await request.getToken(admin);
    const res = await request.getListByPageNum(token, date, search, 1);
    if (res?.code === 1) {
        const list = res.data.list || [];
        if (res.data.count > 10) {
            const pageSum = Math.ceil(res.data.count / 10);
            const promises = [];
            for (let pageNum = 2; pageNum <= pageSum; pageNum++) {
                promises.push(request.getListByPageNum(token, date, search, pageNum).then(data => {
                    list.push(...data.data.list)
                }));
            }
            await Promise.all(promises);
        }

        if (list.length > 0) {
            // 有人未打卡 开始提示
            const nameArr = Array.from(new Set(list.map(item => item.realName)));
            const nameStr = nameArr.join('\r\n');
            const msg = `别忘记晚间打卡~\r\n${nameStr}\r\n你们还未打卡哦`;
            request.send2Group(group, msg).catch(error => {
                // 可拓展为邮件提醒
                console.log(error)
            })
        } else if (list.length === 0) {
            // 所有人都打卡了
            request.send2QQ(qq, `${search}所有人都打卡了`).catch(error => {
                // 可拓展为邮件提醒
                console.log(error)
            })
        }
    }
}

try {
    func()
} catch (error) {
    // 可拓展为邮件提醒
    console.log(error)
}