const axios = require('axios');
const qs = require('qs');
const { QMSGKey } = require('./config.json');

const instance = axios.create({
    baseURL: 'https://yqfw.hebut.edu.cn/api',
    timeout: 15000,
    headers: {
        'Referer': 'https://servicewechat.com/wxd3bdddce3e105289/65/page-frame.html',
        'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 14_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 MicroMessenger/8.0.13(0x18000d38) NetType/WIFI Language/zh_CN',
    }
});

async function getToken(user) {
    return instance.post('/logins', user, {
        headers: {
            code: user.code
        }
    }).then(res => res.data.data.token)
}

function send2Group(group, msg) {
    return axios.post(`https://qmsg.zendee.cn/group/${QMSGKey}`, qs.stringify({
        msg,
        qq: group
    })).then(r => r.data)
}

function send2QQ(qq, msg) {
    return axios.post(`https://qmsg.zendee.cn/send/${QMSGKey}`, qs.stringify({
        msg,
        qq
    })).then(r => r.data)
}
function getListByPageNum(token, date, search, pageNum) {
    return instance.get(`/hm/stats/night/user/list?date=${date}&dayStu=9&leave=9&inFlag=9&fillFlag=&fillType=&type=1&search=${search}&pageNum=${pageNum}`, {
        headers: {
            token
        }
    }).then(r => r.data)
}

module.exports = {
    getToken,
    get: instance.get,
    post: instance.post,
    send2Group,
    send2QQ,
    getListByPageNum
}